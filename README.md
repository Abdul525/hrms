*# HRMS*
[The project is open for anyone who wants to learn or update>>]


**HRMS Description**:
This a Console Application- Python for Employee Data Management.
It's for learning reason.
HR Manager can manage employee data(ADD, UPDATE, DELETE, DISPLAY). While employee can display own info and 
admin shall manage the authentication and assign the roles to the users(HR Manager, Normal Employee). 
By using Python and MySQL in implementation

Need to update:
1- working on the users roles (Admin, HR-Manager, Employee).
2- User Access (authentication).
---------------------------------------------------------------------------------------------------------------



**Some links may help you to understand the basics in Python and the integration with MySQL**
1- HRMS product is a console application in Python using a MySQL database

Here is an example: http://introtopython.org/terminal_apps.html

It could run in a console with a menu, present the user with options.

2- Some tips I found so helpful to get start:
https://www.guru99.com/python-mysql-example.html


3- MySQL with Python Tutorial from here:
https://pythonspot.com/mysql-with-python/
